Use Windows.pkg
Use dfClient.pkg
Use DataDict.pkg
Use dfEntry.pkg
Use dfEnChk.pkg
Use dfCEntry.pkg
Use dfTable.pkg
Use cDbTextEdit.Pkg
Use dfRadio.pkg
Use DfLine.Pkg

Use DatePopUp.sl  // Create Date lookup

Use Contacts.DD
Use Numbers.DD
Use Calls.DD
Use SysFile.DD
Use cDbCJGrid.pkg

Activate_View Activate_Contacts for Contacts

Object Contacts is a dbView

    // This is used to determine if the DDO should display
    // all contacts ("") or just active contacts ("A").
    // The Contacts DDO will get this value via delegation.
    Property String psConstraintValue ""
    
    Set Label to "Contacts"
    Set Location to 5 9
    Set Size to 265 482
    Set pbAutoActivate to True

    Object Contacts_DD is a Contacts_DataDictionary

        // the parent view will have the property psConstraintValue
        // if it is "" show all records, If it is "A" show
        // only active records.
        Procedure OnConstrain
            String sVal
            Get psConstraintValue to sVal
            if (sVal = "A") ;
                Constrain Contacts.Status eq "A"
        End_Procedure

    End_Object    // Contacts_DD

    Object Numbers_DD is a Numbers_DataDictionary
        Set DDO_Server to Contacts_DD
        Set Constrain_File to Contacts.File_Number
    End_Object    // Numbers_DD

    Object Calls_DD is a Calls_DataDictionary
        Set DDO_Server to Contacts_DD
        Set Constrain_File to Contacts.File_Number
    End_Object    // Calls_DD

    Set Main_DD to Contacts_DD
    Set Server to Contacts_DD
    
    Set Verify_Save_msg to (RefFunc(No_Confirmation))    

    Object Contacts_Key is a dbForm
        Entry_Item Contacts.Key
        Set Label to "Key:"
        Set Size to 13 42
        Set Location to 6 49
        Set Label_Col_Offset to 2
        Set Label_Justification_Mode to jMode_Right
    End_Object    // Contacts_Key

    Object Contacts_Status is a dbCheckBox
        Entry_Item Contacts.Status
        Set Label to "Active Contact"
        Set Size to 13 69
        Set Location to 6 110
    End_Object    // Contacts_Status

    Object Contacts_Last_name is a dbForm
        Entry_Item Contacts.Last_name
        Set Label to "Last Name:"
        Set Size to 13 251
        Set Location to 21 49
        Set Label_Col_Offset to 2
        Set Label_Justification_Mode to jMode_Right
    End_Object    // Contacts_Last_name

    Object Contacts_First_name is a dbForm
        Entry_Item Contacts.First_name
        Set Label to "First Name:"
        Set Size to 13 251
        Set Location to 36 49
        Set Label_Col_Offset to 2
        Set Label_Justification_Mode to jMode_Right
    End_Object    // Contacts_First_name

    Object Contacts_Company is a dbForm
        Entry_Item Contacts.Company
        Set Label to "Company:"
        Set Size to 13 251
        Set Location to 50 49
        Set Label_Col_Offset to 2
        Set Label_Justification_Mode to jMode_Right
    End_Object    // Contacts_Company

    Object Contacts_Title is a dbForm
        Entry_Item Contacts.Title
        Set Label to "Title:"
        Set Size to 13 251
        Set Location to 66 49
        Set Label_Col_Offset to 2
        Set Label_Justification_Mode to jMode_Right
    End_Object    // Contacts_Title

    Object Contacts_Address3 is a dbForm
        Entry_Item Contacts.Address1
        Set Label to "Address1:"
        Set Size to 13 251
        Set Location to 81 49
        Set Label_Col_Offset to 2
        Set Label_Justification_Mode to jMode_Right
    End_Object    // Contacts_Address3

    Object Contacts_City is a dbForm
        Entry_Item Contacts.City
        Set Label to "City:"
        Set Size to 13 97
        Set Location to 97 49
        Set Label_Col_Offset to 2
        Set Label_Justification_Mode to jMode_Right
    End_Object    // Contacts_City

    Object Contacts_St is a dbComboForm
        Entry_Item Contacts.St
        Set Label to "St:"
        Set Size to 13 31
        Set Location to 97 161
        Set Form_Border to 0
        Set Label_Col_Offset to 2
        Set Label_Justification_Mode to jMode_Right
        Set Code_Display_Mode to cb_code_display_code
    End_Object    // Contacts_St

    Object Contacts_Postal_code is a dbForm
        Entry_Item Contacts.Postal_code
        Set Label to "Postal Code:"
        Set Size to 13 60
        Set Location to 97 239
        Set Label_Col_Offset to 2
        Set Label_Justification_Mode to jMode_Right
    End_Object    // Contacts_Postal_code

    Object Contacts_Country is a dbForm
        Entry_Item Contacts.Country
        Set Label to "Country:"
        Set Size to 13 121
        Set Location to 112 49
        Set Label_Col_Offset to 2
        Set Label_Justification_Mode to jMode_Right
        
        Procedure Switch
            Boolean bErr
            Get Save_Header to bErr
            Send EnableDtl
            If (not(bErr)) Begin
                Forward Send Switch    
            End
        End_Procedure
        
    End_Object    // Contacts_Country

    Object oNumbersGrid is a cDbCJGrid
        Set Server to Numbers_DD
        Set Size to 121 161
        Set Location to 9 313
        Set pbAllowColumnRemove to False

        Object oNumbers_Phone_Number is a cDbCJGridColumn
            Entry_Item Numbers.Phone_Number
            Set piWidth to 96
            Set psCaption to "Phone#"
        End_Object

        Object oNumbers_Description is a cDbCJGridColumn
            Entry_Item Numbers.Description
            Set piWidth to 145
            Set psCaption to "Description"
            Set pbComboButton to True
            Set pbComboEntryState to False
        End_Object
        
        Function CanEdit Returns Boolean
            Boolean bOld bChange
            Handle hoServer
            Get Main_DD to hoServer
            Get HasRecord of hoServer to bOld
            Get Should_Save of hoServer to bChange
            Function_Return (not(bChange) and bOld)
        End_Function
        
        Procedure OnIdle
            Boolean bCanDo
            Forward Send OnIdle
            Send EnableDtl
        End_Procedure

       
        Function Confirm_Delete_Number Returns Integer
            Integer iRetVal
            Get Confirm "Delete Phone Number?" to iRetVal
            Function_Return iRetVal
        End_Function        
        
        // Define alternative confirmation Messages
        Set Verify_Delete_msg to (RefFunc(Confirm_Delete_Number))        
    End_Object  // oNumbersGrid

    Object oCallsGrid is a cDbCJGrid
        Set Server to Calls_DD
        Set Size to 63 250
        Set Location to 162 15
        
        
        Object oCalls_Call_Date is a cDbCJGridColumn
            Entry_Item Calls.Call_Date
            Set piWidth to 93
            Set psCaption to "Call Date"
            
            // This is called during a calendar popup and it allows the invoking
            // object to set special properties for the popup. In this case we will
            // use it to determine if the calendar should update the invoking object's
            // date as it is change. By default this does not happen, but by setting the
            // prompt object's pbSynchChange property to true, we will get it.        
            Procedure Prompt_Callback Handle hPrompt
                Set pbSynchChange of hPrompt to True
            End_Procedure               
            
        End_Object

        Object oCalls_Purpose is a cDbCJGridColumn
            Entry_Item Calls.Purpose
            Set piWidth to 282
            Set psCaption to "Purpose"
        End_Object
        
        
        // a Next in last column and a Previous in the first column will jump to the editor,  
        // which is what we want here.
        Procedure OnWrappingRow Boolean bForwardWrap Handle hoToColumn Boolean ByRef bWrapRow Boolean ByRef bCancel
            Handle hoDataSource
            Integer iCurRow iNewRow

            // previous/next will not do what they normally do
            Move True to bCancel
                               
            If bForwardWrap Begin
                Send Activate to oCalls_Notes
            End
            Else Begin
                // get current row number
                Get phoDataSource to hoDataSource
                Get SelectedRow of hoDataSource to iCurRow 
            
                Send MoveUpRow

                // check if the row changed, i.e. we are NOT in the first row
                // if we are in the first row and going backwards, we do not move
                Get SelectedRow of hoDataSource to iNewRow
                If (iNewRow <> iCurRow) Begin
                    Send Activate to oCalls_Notes
                End
            End
        End_Procedure


        Function ShouldCommitOnObjectExit Handle hoDestination Returns Boolean
            If (hoDestination = oCalls_Notes) Begin
                Function_Return False
            End
            Function_Return True
        End_Function
        
        Function Confirm_Delete_Call Returns Integer
            Integer iRetVal
            Get Confirm "Delete Call Information?" to iRetVal
            Function_Return iRetVal
        End_Function
        
        // Define alternate confirmation Messages
        Set Verify_Delete_msg to (RefFunc(Confirm_Delete_Call))     

       
        // This will only get sent by the child editor object. It allows it
        // to move to the next row, column zero.
        Procedure NextRow
            Send Activate  // Activate the table
            Send MoveToFirstEnterableColumn
            Send MoveDownRow
        End_Procedure        

        Procedure EndOfRow
            Send Activate // Activate the table
            Send MoveToLastEnterableColumn
        End_Procedure
        
    End_Object  // oCallsGrid


    Object oCalls_Notes is a cDbTextEdit
        Entry_Item Calls.Notes
        Set Server to Calls_DD
        Set Label to "Call Notes"
        Set Size to 51 200
        Set Location to 175 276

        // The Next key will normally send us to the next object. We
        // want it to back up to the dbGrid and go to the next row. We
        // do this by sending the message Next_Row to the dbGrid object.
        Procedure Next
            Send NextRow of oCallsGrid
        End_Procedure
        
        Procedure Previous
            Send EndOfRow of oCallsGrid
        End_Procedure
        
        // For these messages, just do it however the dbGrid does it
        Procedure Request_Save
            send Request_Save of oCallsGrid
        End_Procedure
        
        Procedure Request_Clear
            send Request_Clear of oCallsGrid
        End_Procedure
        
        Procedure Request_Delete
            send Request_Delete of oCallsGrid
        End_Procedure
        
        //  When exiting to the dbGrid, we do not need to perform
        //  a save. When exiting outside of the dbGrid we must save.
        Procedure Exiting Integer iToObject
            Integer iRetVal
            If (iToObject<>oCallsGrid) Begin
                Send Request_Save
                Move (Err) to iRetVal
            End
            Procedure_Return iRetVal
        End_Procedure
        
        
    End_Object    // oCalls_Notes

    Object LineControl2 is a LineControl
        Set Size to 2 465
        Set Location to 149 10
    End_Object    // LineControl2

    Object Textbox2 is a Textbox
        Set Label to "Call Details"
        Set Location to 151 16
        Set Size to 10 36
    End_Object    // Textbox2

    // change the Contacts-dd constraint setting to show all
    Procedure DoShowAllContacts
        set psConstraintValue to ''
        Send Rebuild_Constraints of (Server(Self))
    End_Procedure
    
    // Setting constraints to active only is a little trickier because
    //   we may have an inactive record in the view.
    // Do the following:
    //   If the current contact status is inactive and changed,
    //   stop the process and warn. If it is inactive and not changed
    //   clear it first.
    Procedure DoShowActiveContacts
        Integer iServer
        String sStatus
        Boolean bChanged
    
        Get Server to iServer
        Get Field_Current_Value of iServer Field Contacts.Status to sStatus
    
        If (sStatus <> "A") Begin // if not active we cannot leave this on the screen
            Get Should_Save of iServer to bChanged
            // if changed, stop the constraint change
            If (bChanged) Begin
                Send Stop_Box ;
                    "Can't change filter while changes exist in an inactive Client. Save or clear changes first." ;
                    "Changes Exist"
                
                // reset the radio to radio 0 (show all)
                Set Current_Radio of oDisplayFilter to 0
                Procedure_Return
            End
            Else ;
                Send Request_Clear // if not changed, clear the inactive contact
        End
    
        Set psConstraintValue to 'A'
        Send Rebuild_Constraints of iServer
    End_Procedure  // DoShowActiveContacts
    
    
    Function Save_Header returns Integer
       Integer iServer
       Boolean bChanged bHasRecord
    
       Get Server to iServer  // The Header DDO
    
       // Attempt to save the current record
       // request_save_no_clear does a save without clearing.
       Send Request_Save_No_Clear
    
       // The save succeeded if there are now no changes, and we
       // have a saved record. Should_save tells us if we've got changes.
       // We must check if the DataDictionary's currently has a record
       // If not, we had no save.
       Get Should_Save to bChanged  // is a save still needed?
       Get HasRecord of iServer to bHasRecord
       // if no record or changes still exist, return an error code of 1
       If (bHasRecord=False or bChanged) Begin
          Function_Return 1
       End
       
    End_Function  // Save_Header

    Function CanEditDtl Returns Boolean
        Boolean bOld bChange
        Handle hoServer
        Get Main_DD to hoServer
        Get HasRecord of hoServer to bOld
        Get Should_Save of hoServer to bChange
        Function_Return (not(bChange) and bOld)
    End_Function
    
    Procedure EnableDtl
        Boolean bCanDo
        Get CanEditDtl to bCanDo
        Set Enabled_State of oNumbersGrid to bCanDo
        Set Enabled_State of oCallsGrid to bCanDo
        Set Enabled_State of oCalls_Notes to bCanDo
    End_Procedure

    Object oDisplayFilter is a RadioGroup

        Set Status_Help to "Select whether to display All Contacts or only Active contacts."
        Set Size to 25 195
        Set Location to 235 14
        Set Label to "Display Filter"

        Object oShowAllRadio is a Radio
            Set Label to "Show All Contacts"
            Set Size to 10 74
            Set Location to 10 10
        End_Object

        Object oShowActiveRadio is a Radio
            Set Label to "Show only Active Contacts"
            Set Size to 10 101
            Set Location to 10 90
        End_Object

        Procedure Notify_Select_State Integer iToItem Integer iFromItem
            If (iToItem = 0) Send DoShowAllContacts
            Else Send DoShowActiveContacts
        End_Procedure
        
    End_Object    // oDisplayFilter

    
    
End_Object    // Contacts
