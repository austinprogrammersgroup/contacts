Use DFClient.pkg
Use DFSelLst.pkg
Use Windows.pkg
Use CONTACTS.DD
Use cDbCJGridPromptList.pkg

CD_Popup_Object Contacts_sl is a dbModalPanel
    Set Minimize_Icon to False
    Set Label to "Select Contact"
    Set Location to 4 5
    Set Size to 195 358
    Set piMinSize to 195 358

    Object Contacts_DD is a Contacts_DataDictionary
    End_Object    // Contacts_DD

    Set Main_DD to Contacts_DD
    Set Server to Contacts_DD

    Object oSelList is a cDbCJGridPromptList
        Set Size to 157 346
        Set Location to 12 5
        Set peHorizontalGridStyle to xtpGridNoLines
        Set pbAllowColumnRemove to False
        Set peAnchors to anAll
        Set pbAutoServer to True

        Object oCONTACTS_KEY is a cDbCJGridColumn
            Entry_Item CONTACTS.KEY
            Set piWidth to 27
            Set psCaption to "ID"
        End_Object

        Object oCONTACTS_LAST_NAME is a cDbCJGridColumn
            Entry_Item CONTACTS.LAST_NAME
            Set piWidth to 138
            Set psCaption to "Last Name"
        End_Object

        Object oCONTACTS_FIRST_NAME is a cDbCJGridColumn
            Entry_Item CONTACTS.FIRST_NAME
            Set piWidth to 138
            Set psCaption to "First Name"
        End_Object

        Object oCONTACTS_COMPANY is a cDbCJGridColumn
            Entry_Item CONTACTS.COMPANY
            Set piWidth to 175
            Set psCaption to "Company"
        End_Object

        Object oCONTACTS_STATUS is a cDbCJGridColumn
            Entry_Item CONTACTS.STATUS
            Set piWidth to 41
            Set psCaption to "Active"
            Set pbCheckbox to True
            Set peIconAlignment to xtpAlignmentIconCenter
        End_Object
    End_Object

    Object oOK_bn is a Button
        Set Label to "&Ok"
        Set Location to 177 197
        Set peAnchors to anBottomRight
        Set Default_State to True

        Procedure OnClick
            Send Ok of oSelList
        End_Procedure

    End_Object    // oOK_bn

    Object oCancel_bn is a Button
        Set Label to "&Cancel"
        Set Location to 177 250
        Set peAnchors to anBottomRight

        Procedure OnClick
            Send Cancel of oSelList
        End_Procedure

    End_Object    // oCancel_bn

    Object oSearch_bn is a Button
        Set Label to "&Search..."
        Set Location to 177 303
        Set peAnchors to anBottomRight

        Procedure OnClick
            Send Search of oSelList
        End_Procedure

    End_Object    // oSearch_bn

    On_Key Key_Alt+Key_O Send KeyAction of oOk_bn
    On_Key Key_Alt+Key_C Send KeyAction of oCancel_bn
    On_Key Key_Alt+Key_S Send KeyAction of oSearch_bn

CD_End_Object    // Contacts_sl
