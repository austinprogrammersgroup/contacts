Use dfrptvw.pkg
Use DataDict.pkg
Use Windows.pkg
Use dfRadio.pkg
Use cWinReport2.pkg
Use CONTACTS.DD

ACTIVATE_VIEW Activate_oContact_List FOR oContact_List

Object oContact_List is a ReportView

    Property Integer IndexToUse 1
    
    Set Label to "Contact List"
    Set Location to 6 6
    Set Size to 251 192

    Object oContacts_DD is a Contacts_DataDictionary

        Property String psStartLast_Name
        Property String psStopLast_Name
        Property String psStartCompany
        Property String psStopCompany
        Property String psStartSt
        Property String psStopSt
        
        Procedure OnConstrain
            String sStart sStop
        
            Get psStartLast_Name To sStart
            Get psStopLast_Name To sStop
        
            Case Begin
                Case (sStart <> "" and sStop <> "")
                    Constrain Contacts.Last_Name between sStart and sStop
                    Case Break
                Case (sStart <> "")
                    Constrain Contacts.Last_Name GE sStart
                    Case Break
                Case (sStop <> "")
                    Constrain Contacts.Last_Name LE sStop
            Case End
        
            Get psStartCompany To sStart
            Get psStopCompany To sStop
        
            Case Begin
                Case (sStart <> "" and sStop <> "")
                    Constrain Contacts.Company between sStart and sStop
                    Case Break
                Case (sStart <> "")
                    Constrain Contacts.Company GE sStart
                    Case Break
                Case (sStop <> "")
                    Constrain Contacts.Company LE sStop
            Case End
        
            Get psStartSt To sStart
            Get psStopSt To sStop
        
            Case Begin
                Case (sStart <> "" and sStop <> "")
                    Constrain Contacts.St between sStart and sStop
                    Case Break
                Case (sStart <> "")
                    Constrain Contacts.St GE sStart
                    Case Break
                Case (sStop <> "")
                    Constrain Contacts.St LE sStop
            Case End
        End_Procedure // OnConstrain
        
    End_Object    // oContacts_DD

    Set Main_DD to oContacts_DD

    Object oLast_NameSelection is a Group
        Set Size to 35 179
        Set Location to 8 8
        Set Label to "Select Last Name"
        Object oSelStart is a Form
            Set Label to "From:"
            Set Size to 13 60
            Set Location to 13 30
            Set Status_Help to "First value in selection range."
            Set Label_Col_Offset to 2
            Set Label_Justification_Mode to jMode_Right
        End_Object    // oSelStart

        Object oSelStop is a Form
            Set Label to "To:"
            Set Size to 14 60
            Set Location to 13 111
            Set Status_Help to "Last value in selection range."
            Set Label_Col_Offset to 2
            Set Label_Justification_Mode to jMode_Right
        End_Object    // oSelStop

        Function StartValue returns String
            String sValue
            Get Value of oSelStart to sValue
            Function_Return sValue
        End_Function // StartValue
        
        Function StopValue returns String
            String sValue
            Get Value of oSelStop to sValue
            Function_Return sValue
        End_Function // StopValue
        
    End_Object    // oLast_NameSelection

    Object oCompanySelection is a Group
        Set Size to 35 179
        Set Location to 48 8
        Set Label to "Select Company"
        Object oSelStart is a Form
            Set Label to "From:"
            Set Size to 13 60
            Set Location to 13 30
            Set Status_Help to "First value in selection range."
            Set Label_Col_Offset to 2
            Set Label_Justification_Mode to jMode_Right
        End_Object    // oSelStart

        Object oSelStop is a Form
            Set Label to "To:"
            Set Size to 14 60
            Set Location to 13 111
            Set Status_Help to "Last value in selection range."
            Set Label_Col_Offset to 2
            Set Label_Justification_Mode to jMode_Right
        End_Object    // oSelStop

        Function StartValue returns String
            String sValue
            Get Value of oSelStart to sValue
            Function_Return sValue
        End_Function // StartValue
        
        Function StopValue returns String
            String sValue
            Get Value of oSelStop to sValue
            Function_Return sValue
        End_Function // StopValue
        
    End_Object    // oCompanySelection

    Object oStSelection is a Group
        Set Size to 35 179
        Set Location to 88 8
        Set Label to "Select State"
        Object oSelStart is a Form
            Set Label to "From:"
            Set Size to 13 60
            Set Location to 13 30
            Set Status_Help to "First value in selection range."
            Set Label_Col_Offset to 2
            Set Label_Justification_Mode to jMode_Right
        End_Object    // oSelStart

        Object oSelStop is a Form
            Set Label to "To:"
            Set Size to 14 60
            Set Location to 13 111
            Set Status_Help to "Last value in selection range."
            Set Label_Col_Offset to 2
            Set Label_Justification_Mode to jMode_Right
        End_Object    // oSelStop

        Function StartValue returns String
            String sValue
            Get Value of oSelStart to sValue
            Function_Return sValue
        End_Function // StartValue
        
        Function StopValue returns String
            String sValue
            Get Value of oSelStop to sValue
            Function_Return sValue
        End_Function // StopValue
        
    End_Object    // oStSelection

    Object oOrderByRadioGroup is a RadioGroup
        Set Size to 49 177
        Set Location to 128 8
        Set Label to "Order Records by"
        Object oIndex1 is a Radio
            Set Label to "Last Name + First Name + Company + Key"
            Set Size to 10 149
            Set Location to 10 5
        End_Object    // oIndex1

        Object oIndex2 is a Radio
            Set Label to "Company + Last Name + Key"
            Set Size to 10 108
            Set Location to 22 5
        End_Object    // oIndex2

        Object oIndex4 is a Radio
            Set Label to "Status + Last Name + Key"
            Set Size to 10 99
            Set Location to 34 6
        End_Object    // oIndex4

        Procedure Notify_Select_State Integer iToItem Integer iFromItem
            Forward Send Notify_Select_State iToItem iFromItem
        
            Case Begin
                // For the first two options, iToItem will one less than
                // the index number that needs to be used
                Case (iToItem = 0)
                Case (iToItem = 1)
                    move (iToItem + 1) to iToItem
                    Case Break
        
                // For the last option, iToItem will be 2 and
                // the index number to be used is 4
                Case (iToItem = 2)
                    move 4 to iToItem
        
            Case End
        
            set IndexToUse to iToItem
        
            //for augmentation
        End_Procedure // Notify_Select_State
        
        // If you set Current_radio, you must set this after the
        // radio objects have been created AND after Notify_select_State has been
        // created. (i.e. Set in bottom-code at end!!)
        Set Current_Radio To 0
        
    End_Object    // oOrderByRadioGroup

    Object oPrintTo is a RadioGroup
        Set Size to 38 118
        Set Location to 183 8
        Set Label to "Send Report to"
        Object oRadio1 is a Radio
            Set Label to "Report Viewer"
            Set Size to 10 61
            Set Location to 12 6
            Set Status_Help to "Prints the report to screen"
        End_Object    // oRadio1

        Object oRadio2 is a Radio
            Set Label to "Printer"
            Set Size to 10 42
            Set Location to 24 6
            Set Status_Help to "Prints the report to printer"
        End_Object    // oRadio2

        Function IsToPrinter Returns boolean
            Integer iRadio
            Get Current_Radio to iRadio
            Function_Return (iRadio=1)
        End_Function // IsToPrinter
        
    End_Object    // oPrintTo

    Object oBtnPrinterSetup is a Button
        Set Label to "Printer Setup"
        Set Location to 206 137
        Set Status_Help to "Set up printer for report"

        Procedure OnClick
            Boolean bSetupOk
            Get DFPrintSetupDialog of oReport to bSetupOk
        End_Procedure

    End_Object    // oBtnPrinterSetup

    Object oBtnPrint is a Button
        Set Label to "Print"
        Set Location to 233 84
        Set Status_Help to "Print the Selected report"
        Set Default_State to True

        Procedure OnClick
            // Set the order acording to index picked by user
            send SetOrdering of oReport
        
            Send StartReport
        End_Procedure

    End_Object    // oBtnPrint

    Object oBtnCancel is a Button
        Set Label to "Cancel"
        Set Location to 233 137
        Set Status_Help to "Close this Panel"

        Procedure OnClick
            Send Close_Panel
        End_Procedure

    End_Object    // oBtnCancel

    Object oReport is a cWinReport2
        Set Report_Title to "Contact List"

        // Set DDO, Index and breaks for the report
        Set Server to oContacts_DD
        
        // Gets value selected by the user (saved in IndexToUse property)
        // and sets Ordering
        Procedure SetOrdering
            Integer iOrder
        
            get IndexToUse to iOrder
            Set Ordering to iOrder
        End_Procedure
        
        // Report_Breaks file.field // no breaks in this report
        
        Function Starting_Main_Report Returns Integer
            Boolean bErr
        
            Send DFSetMetrics wpm_cm
            Send DFSetmargins 1 1 1 1
        
            //Send DFSetLandscape false // This can be used to force a page orientation
            Forward Get Starting_Main_Report to bErr
        
            Function_Return bErr
        End_Function // Starting_Main_Report
        
        // Page_Top is printed first at the top margin of each page
        Procedure Page_Top
            String  sFont
            Integer iFontSize
        
            Move "Arial" to sFont
            Move 8 to iFontSize
        
            DFFont sFont
            DFFontSize iFontSize
            DFBeginHeader DFPageTop
                DFHeaderFrame Hdr_NoFrame
                DFHeaderPos   Hdr_Left
                DFWriteLn ("Page:" * "#pagecount#") (Font_Right)
            DFEndHeader
        
        End_Procedure // Page_Top
        
        // Report_Header is printed once in a report after Page_Top
        //Procedure Report_Header
        //    String  sFont
        //    Integer iFontSize
        //    Move "Arial" to sFont
        //    Move 8 to iFontSize
        //    DFFont sFont
        //    DFFontSize iFontSize
        //    DFBeginHeader  DFReportHeader
        //        DFHeaderFrame  Hdr_Margins 0.01
        //        DFHeaderPos    Hdr_Left
        //        DFHeaderMargin Hm_BottomOuter 0.08
        //        DFWrite     "Report_Header" (Font_Bold+Font_Center)
        //    DFEndHeader
        //End_Procedure
        
        Procedure Page_Header
            Integer iFill iBorder iStyle
            String  sFont sReportTitle
            Integer iFontSize
        
            Move "Arial" to sFont
            Move 16 to iFontSize
            Move (Font_Bold + Font_Italic + Font_Center + rgb_White) to iStyle
            Move (rgb_dGrey) to iFill
            Move (rgb_dGrey) to iBorder
            Get Report_Title to sReportTitle
        
            DFFont sFont
            DFFontSize iFontSize
            DFBeginHeader DFPageHeader
                DFHeaderPos   Hdr_Left
                DFHeaderFrame Hdr_Margins 0.01 iBorder iFill
                DFWriteln sReportTitle iStyle
                DFHeaderMargin HM_BottomOuter 0.08
            DFEndHeader
        
        End_Procedure // Page_Header
        
        Procedure Page_Title
            String  sFont
            Integer iFontSize iHeaderStyle iFill iBorder
        
            Move "Arial" to sFont
            Move 12 to iFontSize
            Move (Font_Bold + rgb_dBlue) to iHeaderStyle
            Move (rgb_Grey) to iFill
            Move (rgb_Grey) to iBorder
        
            DFFont sFont
            DFFontSize iFontSize
            DFBeginHeader DFPageTitle
                DFHeaderPos   Hdr_Left
                DFHeaderFrame Hdr_Margins 0.01 iBorder iFill
                DfHeaderMargin HM_TopInner    0.04
                DfHeaderMargin HM_BottomInner 0.04
                DFHeaderMargin HM_BottomOuter 0.08
        
                DfWritePos "ID"                           (0.08 + 1.02) (iHeaderStyle + Font_Right) -1 1.02
                DfWritePos "Last Name"                    2 iHeaderStyle -1 6.82
                DfWritePos "First Name"                   6 iHeaderStyle -1 6.82
                DfWritePos "Company"                      10 iHeaderStyle -1 5.11
                DfWritePos "Status"                       16 iHeaderStyle -1 1.30
                DfWriteLn
            DFEndHeader
        
        End_Procedure // Page_Title
        
        Procedure Body
            String  sFont sStatus
            Integer iFontSize iStyle
        
            Move "Arial" to sFont
            Move 10 to iFontSize
            Move (font_default) to iStyle
        
            DFFont sFont
            DFFontSize iFontSize
        
            DfWritePos Contacts.Key                   (0.08 + 1.02) (iStyle + Font_Right) 0 1.02
            DfWritePos Contacts.Last_Name             2 iStyle -1 6.82
            DfWritePos Contacts.First_Name            6 iStyle -1 6.82
            DfWritePos Contacts.Company               10 iStyle -1 5.11
        
            // The content of Contacts.Status can be "A" or "I". We will print
            // "Active" when "A" is the value and "Inactive" when "I" is the value
            Move (If(Contacts.Status="A","Active","Inactive")) to sStatus
            DfWritePos sStatus                       16 iStyle -1 1.22
        
            DfWriteLn
        
        End_Procedure // Body
        
        // Totals appear at the end of the report before Report_Footer
        //Procedure Total
        //    String  sFont
        //    Integer iFontSize
        //    Move "Arial" to sFont
        //    Move 8 to iFontSize
        //    DFFont sFont
        //    DFFontSize iFontSize
        //    DFBeginHeader  DFTotal
        //        DFHeaderFrame  Hdr_Margins 0.01
        //        DFHeaderPos    Hdr_Left
        //        DFHeaderMargin Hm_BottomOuter 0.08
        //        DFWrite     "Total" (Font_Bold+Font_Center)
        //    DFEndHeader
        //End_Procedure
        
        // Report_Footer is printed once on the last page before Page_Footer
        //Procedure Report_Footer
        //    String  sFont
        //    Integer iFontSize
        //    Move "Arial" to sFont
        //    Move 8 to iFontSize
        //    DFFont sFont
        //    DFFontSize iFontSize
        //    DFBeginHeader DFReportFooter
        //        DFHeaderFrame  Hdr_Margins 0.01
        //        DFHeaderPos    Hdr_Left
        //        DFHeaderMargin Hm_BottomOuter 0.08
        //        DFWrite     "Report_Footer" (Font_Bold+Font_Center)
        //    DFEndHeader
        //End_Procedure
        
        Procedure Page_Footer
            String sStart sStop sText
            String  sFont
            Integer iFontSize iStyle
            Handle  hoDD
        
            Move "Arial" to sFont
            Move 8 to iFontSize
            Move (font_default) to iStyle
        
            Get Server To hoDD
        
            DFFont sFont
            DFFontSize iFontSize
            DFBeginHeader DFPageFooter
                DfHeaderFrame hdr_NoFrame
                DfHeaderPos   hdr_Left
        
                Get psStartLast_Name of hoDD to sStart
                Get psStopLast_Name  of hoDD to sStop
                Get SelectionCriteraDescription "Contacts.Last_Name" sStart sStop to sText
                If (sText<>"") begin
                    DfWriteLnPos sText 0.1
                end
        
                Get psStartCompany of hoDD to sStart
                Get psStopCompany  of hoDD to sStop
                Get SelectionCriteraDescription "Contacts.Company" sStart sStop to sText
                If (sText<>"") begin
                    DfWriteLnPos sText 0.1
                end
        
                Get psStartSt of hoDD to sStart
                Get psStopSt  of hoDD to sStop
                Get SelectionCriteraDescription "Contacts.St" sStart sStop to sText
                If (sText<>"") begin
                    DfWriteLnPos sText 0.1
                end
        
            DFEndHeader
        End_Procedure // Page_Footer
        
        // Page_Bottom is printed last at the bottom margin of each page
        Procedure Page_Bottom
            String sFont
            Integer iFontSize iBorder iStyle
            DateTime dtDT
        
            Move (CurrentDateTime()) to dtDT
        
            Move "Arial" to sFont
            Move 8 to iFontSize
            Move (font_default) to iStyle
            Move (rgb_dGrey) to iBorder
        
            DFFont sFont
            DFFontSize iFontSize
            DFBeginHeader DFPageBottom
                DFHeaderFrame Hdr_Margins 0.01 iBorder
                DFHeaderPos Hdr_Left
                DFWriteln ("Report Printed on" * string(dtDT)) (iStyle+Font_Center)
            DFEndHeader
        
        End_Procedure // Page_Bottom
        
        // Returns String with description of selection ranges
        // Pass: Selection title, start value and end value. Return: description
        Function SelectionCriteraDescription String sTitle String sStart String sStop returns String
            String sText
            
            If (sStart=""  AND sStop="")  Function_Return ""
            If (sStart="0" AND sStop="0") Function_Return ""
            If (sStart<>"" AND sStop<>"") Begin
                Move ("between" * sStart * "and" * sStop) to sText
            End
            Else If (sStart > "") Begin
                Move ("starting at" * sStart) to sText
            End
            Else if (sStop > "") Begin
                Move ("up to" * sStop) to sText
            End
            Function_Return (sTitle * sText)
        End_Function
        
    End_Object    // oReport

    Procedure StartReport
        Boolean bToPrinter
        Handle hoDD
        String sValue
        String sStart sStop
    
        Get Server of oReport To hoDD
    
        // Set all selection critera properties
        Get StartValue of oLast_NameSelection to sValue
        Set psStartLast_Name of hoDD To sValue
    
        Get StopValue of oLast_NameSelection to sValue
        Set psStopLast_Name of hoDD To sValue
    
        Get StartValue of oCompanySelection to sValue
        Set psStartCompany of hoDD To sValue
    
        Get StopValue of oCompanySelection to sValue
        Set psStopCompany of hoDD To sValue
    
        Get StartValue of oStSelection to sValue
        Set psStartSt of hoDD To sValue
    
        Get StopValue of oStSelection to sValue
        Set psStopSt of hoDD To sValue
    
        // determine if direct print
        Get IsToPrinter of oPrintTo to bToPrinter
        Set OutPut_Device_Mode of oReport ;
            to (If(bToPrinter, PRINT_TO_PRINTER, PRINT_TO_WINDOW))
    
        // run the report
        Send Run_Report of oReport
    End_Procedure // StartReport
    
End_Object    // oContact_List
