// Properties to save some selections
Property Integer pStatusMode 0 // 0=all, 1=active, 2=inactive
Property Integer piCount 0 // if zero, the body has not printed yet

Use dfrptvw.pkg
Use DataDict.pkg
Use Windows.pkg
Use dfRadio.pkg
Use cWinReport2.pkg
Use CONTACTS.DD
Use NUMBERS.DD
Use CALLS.DD

ACTIVATE_VIEW Activate_oContact_Detail FOR oContact_Detail

Object oContact_Detail is a ReportView
    Set Label to "Contact Detail"
    Set Location to 6 6
    Set Size to 229 316

    Object oContacts_DD is a Contacts_DataDictionary

        Property String psStartLast_Name
        Property String psStopLast_Name
        Property String psStartCompany
        Property String psStopCompany
        
        Procedure OnConstrain
            String sStart sStop
            Integer iMode
        
            Get psStartLast_Name To sStart
            Get psStopLast_Name To sStop
        
            Case Begin
                Case (sStart <> "" and sStop <> "")
                    Constrain Contacts.Last_Name between sStart and sStop
                    Case Break
                Case (sStart <> "")
                    Constrain Contacts.Last_Name GE sStart
                    Case Break
                Case (sStop <> "")
                    Constrain Contacts.Last_Name LE sStop
            Case End
        
            Get psStartCompany To sStart
            Get psStopCompany To sStop
        
            Case Begin
                Case (sStart <> "" and sStop <> "")
                    Constrain Contacts.Company between sStart and sStop
                    Case Break
                Case (sStart <> "")
                    Constrain Contacts.Company GE sStart
                    Case Break
                Case (sStop <> "")
                    Constrain Contacts.Company LE sStop
            Case End
        
            Get pStatusMode to iMode
            If (iMode = 1) ; // print active only
                Constrain Contacts.STATUS eq "A"
            Else if (iMode = 2) ; // print inactive only
                Constrain Contacts.STATUS eq "I"
            // if 0, we print all
        
        End_Procedure // OnConstrain
        
    End_Object    // oContacts_DD

    Object oNumbers_DD is a Numbers_DataDictionary
        Set DDO_Server to oContacts_DD
        Set Constrain_File to Contacts.File_Number
    End_Object    // oNumbers_DD

    Object oCalls_DD is a Calls_DataDictionary
        Set DDO_Server to oContacts_DD
        Set Constrain_File to Contacts.File_Number
    End_Object    // oCalls_DD

    Set Main_DD to oNumbers_DD

    Object oLast_NameSelection is a Group
        Set Size to 34 304
        Set Location to 8 8
        Set Label to "Select Last Name"
        Object oSelStart is a Form
            Set Label to "From:"
            Set Size to 13 121
            Set Location to 13 30
            Set Status_Help to "First value in selection range."
            Set Label_Col_Offset to 2
            Set Label_Justification_Mode to jMode_Right
            Set Prompt_Button_Mode to pb_PromptOn

            Set Prompt_object to (Contacts_sl(self))
            
            Procedure Prompt_Callback integer hSL
                Set peUpdateMode of hSL to umPromptValue
                Set piUpdateColumn of hSL to 1  // column 1 is name
            End_Procedure

        End_Object    // oSelStart

        Object oSelStop is a Form
            Set Label to "To:"
            Set Size to 14 121
            Set Location to 13 173
            Set Status_Help to "Last value in selection range."
            Set Label_Col_Offset to 2
            Set Label_Justification_Mode to jMode_Right
            Set Prompt_Button_Mode to pb_PromptOn
            Set Prompt_object to (Contacts_sl(self))
            
            Procedure Prompt_Callback integer hSL
                Set peUpdateMode of hSL to umPromptValue
                Set piUpdateColumn of hSL to 1  // column 1 is name
            End_Procedure
 
        End_Object    // oSelStop

        Function StartValue returns String
            String sValue
            Get Value of oSelStart to sValue
            Function_Return sValue
        End_Function // StartValue
        
        Function StopValue returns String
            String sValue
            Get Value of oSelStop to sValue
            Function_Return sValue
        End_Function // StopValue
        
    End_Object    // oLast_NameSelection

    Object oCompanySelection is a Group
        Set Size to 34 304
        Set Location to 48 8
        Set Label to "Select Company"
        Object oSelStart is a Form
            Set Label to "From:"
            Set Size to 13 121
            Set Location to 13 30
            Set Status_Help to "First value in selection range."
            Set Label_Col_Offset to 2
            Set Label_Justification_Mode to jMode_Right
            Set Prompt_Button_Mode to pb_PromptOn
            Set Prompt_object to (Contacts_sl(self))
            
            Procedure Prompt_Callback Integer hSL
                Set peUpdateMode of hSL to umPromptValue
                Set piUpdateColumn of hSL to 3 // column 3 is company                
            End_Procedure

        End_Object    // oSelStart

        Object oSelStop is a Form
            Set Label to "To:"
            Set Size to 14 121
            Set Location to 14 173
            Set Status_Help to "Last value in selection range."
            Set Label_Col_Offset to 2
            Set Label_Justification_Mode to jMode_Right
            Set Prompt_Button_Mode to pb_PromptOn
            Set Prompt_object to (Contacts_sl(self))
            
            Procedure Prompt_Callback integer hSL
                Set peUpdateMode of hSL to umPromptValue
                Set piUpdateColumn of hSL to 3 // column 3 is company 
            End_Procedure

        End_Object    // oSelStop

        Function StartValue returns String
            String sValue
            Get Value of oSelStart to sValue
            Function_Return sValue
        End_Function // StartValue
        
        Function StopValue returns String
            String sValue
            Get Value of oSelStop to sValue
            Function_Return sValue
        End_Function // StopValue
        
    End_Object    // oCompanySelection

    Object oSelectStatusRG is a RadioGroup
        Set Size to 36 147
        Set Location to 90 8
        Set Label to "Status to Print"
        Object oAll is a Radio
            Set Label to "All"
            Set Size to 10 24
            Set Location to 16 11
        End_Object    // oAll

        Object oActive is a Radio
            Set Label to "Active"
            Set Size to 10 37
            Set Location to 16 45
        End_Object    // oActive

        Object oInactive is a Radio
            Set Label to "Inactive"
            Set Size to 10 42
            Set Location to 16 93
        End_Object    // oInactive

        Procedure Notify_Select_State Integer iToItem Integer iFromItem
            Forward Send Notify_Select_State iToItem iFromItem
        
            Set pStatusMode to iToItem
        End_Procedure // Notify_Select_State
        
        // Default is ALL
        Set Current_Radio to 0
        
    End_Object    // oSelectStatusRG

    Object oOrderRG is a RadioGroup
        Set Size to 36 147
        Set Location to 91 165
        Set Label to "Order by"
        Object oKey is a Radio
            Set Label to "Key"
            Set Size to 10 29
            Set Location to 16 9
        End_Object    // oKey

        Object oLast_Name is a Radio
            Set Label to "Last Name"
            Set Size to 10 51
            Set Location to 16 41
        End_Object    // oLast_Name

        Object oCompany is a Radio
            Set Label to "Company"
            Set Size to 10 46
            Set Location to 16 96
        End_Object    // oCompany

        // Default is Last_Name
        Set Current_Radio to 1

    End_Object    // oOrderRG

    Object oPrintOptions is a Group
        Set Size to 74 303
        Set Location to 132 7
        Set Label to "Printing Options"
        Object oPrintTo is a RadioGroup
            Set Size to 41 117
            Set Location to 10 173
            Set Label to "Send Report to"
            Object oReportViewer is a Radio
                Set Label to "Report Viewer"
                Set Size to 10 61
                Set Location to 12 6
                Set Status_Help to "Prints the report to screen"
            End_Object    // oReportViewer

            Object oPrinter is a Radio
                Set Label to "Printer"
                Set Size to 10 42
                Set Location to 24 6
                Set Status_Help to "Prints the report to printer"
            End_Object    // oPrinter

            Function IsToPrinter Returns boolean
                Integer iRadio
                Get Current_Radio to iRadio
                Function_Return (iRadio=1)
            End_Function // IsToPrinter
            
        End_Object    // oPrintTo

        Object oBtnPrinterSetup is a Button
            Set Label to "Printer Setup"
            Set Location to 55 239
            Set Status_Help to "Set up printer for report"

            Procedure OnClick
                Boolean bSetupOk
                Get DFPrintSetupDialog of oReport to bSetupOk
            End_Procedure

        End_Object    // oBtnPrinterSetup

        Object oNewPage is a CheckBox
            Set Label to "Create new page for each Contact"
            Set Size to 10 125
            Set Location to 18 11

            Function CreateNewPage returns Boolean
                Boolean bChecked
            
                Get Checked_State To bChecked
            
                Function_Return bChecked
            End_Function // CreateNewPage
            
        End_Object    // oNewPage

    End_Object    // oPrintOptions

    Object oBtnPrint is a Button
        Set Label to "Print"
        Set Location to 211 208
        Set Status_Help to "Print the Selected report"
        Set Default_State to True
      
        Procedure OnClick
            Integer iMode
        
            // Set sort order of the report according to the selection made by user
            Get Current_Radio of oOrderRG 0 to iMode
            If (iMode = 0)      Set Ordering of oReport to 5 // Key
            else If (iMode = 1) Set Ordering of oReport to 1 // Name
            Else                Set Ordering of oReport to 2 // Company
        
            Send StartReport
        End_Procedure

    End_Object    // oBtnPrint

    Object oBtnCancel is a Button
        Set Label to "Cancel"
        Set Location to 211 261
        Set Status_Help to "Close this Panel"

        Procedure OnClick
            Send Close_Panel
        End_Procedure

    End_Object    // oBtnCancel

    Object oReport is a cWinReport2

        Set Report_Title to "Contact Details"
        
        Object oNumbersList is a cWinReport2
         
            Property Boolean pbFirstLine True
            
            // Set DDO, and Index for the report
            Set Server to oNumbers_DD
            Set Ordering to 1
            
            // we want a blank line for the first record
            Function Starting_Report Returns Integer
                Set pbFirstLine to True
            End_Function
            
            // Print Phone Numbers
            Procedure Body
                Boolean bFirstLine
                String sPhoneMask sPhone
            
                Get pbFirstline to bFirstLine
                Get Field_Mask of oNumbers_DD Field Numbers.Phone_Number to sPhoneMask
                Get Format_String Numbers.Phone_Number sPhoneMask to sPhone
            
                // Format sPhone
                DFFont "Arial"
                DFFontSize 10
            
                If bFirstLine Begin
                    DFWriteLn
                    Set pbFirstLine to False
                End
            
                DFWritePos Numbers.Description 2 (Font_Italic)
                DFWritePos sPhone              7 (Font_Default)
                DFWriteLn
            End_Procedure  // Body

        End_Object    // oNumbersList

        Object oCallsList is a cWinReport2
            
            // Set DDO and Index for the report
            Set Server to oCalls_DD
            Set Ordering to 1
            
            Property Boolean pbFirstLine True
            
            // we want a blank line for the first record
            Function Starting_Report Returns Integer
                Set pbFirstLine to True
            End_function
            
            // Print Calls
            Procedure Body
                Boolean bFirstLine
            
                Get pbFirstline to bFirstLine
            
                DFFont "Arial"
                DFFontSize 8
            
                If bFirstLine Begin
                    Set pbFirstLine to False
                    DFWriteLn
                    DFWritePos "Call Date" 2  (Font_Bold + rgb_dBlue)
                    DFWritePos "Purpose"   7  (Font_Bold + rgb_dBlue)
                    DFWritePos "Notes"     11 (Font_Bold + rgb_dBlue)
                    DFWriteLn
                    DFWriteLine DFGR_CURRLINE 2 DFGR_RB_MARGIN
                End
            
                DFWritePos Calls.Call_Date 2  (Font_Italic + Font_Bold)
                DFWritePos Calls.Purpose   7  (Font_Default)
                DFWritePos Calls.Notes     11 (Font_Default)
                DFWriteLn
            End_Procedure  // Body

        End_Object    // oCallsList

        // Set DDO, Index and breaks for the report
        Set Server to oContacts_DD
        
        // Set Ordering to 1  // index is being set in oBtnPrint
        // Report_Breaks file.field // no breaks in this report
        
        Function Starting_Main_Report Returns Integer
            Boolean bErr
        
            Send DFSetMetrics wpm_cm
            Send DFSetmargins 1 1 1 1
            Send DFSetLandscape true // This can be used to force a page orientation
        
            Forward Get Starting_Main_Report to bErr
        
            Function_Return bErr
        End_Function // Starting_Main_Report
        
        // Page_Top is printed first at the top margin of each page
        Procedure Page_Top
            String  sFont
            Integer iFontSize
        
            Move "Arial" to sFont
            Move 10 to iFontSize
        
            DFFont sFont
            DFFontSize iFontSize
            DFBeginHeader DFPageTop
                DFHeaderFrame Hdr_NoFrame
                DFHeaderPos   Hdr_Left
                DFWriteLn ("Page:" * "#pagecount#") (Font_Right)
            DFEndHeader
        End_Procedure // Page_Top
        
        Procedure Page_Header
            Integer iFill iBorder iStyle iFontSize
            String  sFont sReportTitle
        
            Move "Arial" to sFont
            Move 16 to iFontSize
            Move (Font_bold + rgb_White + Font_Center) to iStyle
            Move (rgb_dGrey) to iFill
            Move (rgb_dGrey) to iBorder
            Get Report_Title to sReportTitle
        
            DFFont sFont
            DFFontSize iFontSize
            DFBeginHeader DFPageHeader
                DFHeaderPos   Hdr_Left
                DFHeaderFrame Hdr_Margins 0.01 iBorder iFill
                DFWriteln sReportTitle iStyle
                DFHeaderMargin HM_BottomOuter 0.08
            DFEndHeader
        
        End_Procedure // Page_Header
        
        Procedure Page_Title
            String  sFont
            Integer iFontSize iHeaderStyle iFill iBorder
        
            Move "Arial" to sFont
            Move 10 to iFontSize
            Move (Font_Bold + rgb_dBlue) to iHeaderStyle
            Move (rgb_Grey) to iFill
            Move (rgb_Grey) to iBorder
        
            DFFont sFont
            DFFontSize iFontSize
            DFBeginHeader DFPageTitle
                DFHeaderPos   Hdr_Left
                DFHeaderFrame Hdr_Margins 0.01 iBorder iFill
                DfHeaderMargin HM_TopInner    0.04
                DfHeaderMargin HM_BottomInner 0.04
                DFHeaderMargin HM_BottomOuter 0.08
        
                DfWritePos "Key"                          (0.08 + 1.27) (iHeaderStyle + Font_Right) -1 1.27
                DfWritePos "Last Name"                    2 iHeaderStyle -1 8.53
                DfWritePos "First Name"                   7 iHeaderStyle -1 8.53
                DfWritePos "Company"                      15 iHeaderStyle -1 6.39
                DfWritePos "Status"                       22 iHeaderStyle -1 1.53
                DfWriteLn
            DFEndHeader
        
        End_Procedure // Page_Title
        
        Procedure Body
            String  sFont sStatus
            Integer iFontSize iStyle iCount
            Boolean bNewPage
        
            Move "Arial" to sFont
            Move 10 to iFontSize
            Move (font_default) to iStyle
        
            // by using this property and breaking the page before its body is printed, we avoid
            // having a blank page at the end
            get piCount to iCount
            if (iCount = 0);
                set piCount to 1
            Else Begin
                get CreateNewPage of oNewPage to bNewPage
                // If user chose to print each contact on a separate page, break the page
                If (bNewPage) ;
                    Send DFNew_Page
                Else ;
                    DFWriteLn
            End
        
            // Update the information box (sentinel) for each body record
            Send Update_Status ("Reading" * Contacts.First_Name * Contacts.Last_name)
        
            DFFont sFont
            DFFontSize iFontSize
        
            DfWritePos Contacts.Key                   (0.08 + 1.27) (Font_Bold + iStyle + Font_Right) 0 1.27
            DfWritePos Contacts.Last_Name             2 (Font_Bold + iStyle) -1 8.53
            DfWritePos Contacts.First_Name            7 (Font_Bold + iStyle) -1 8.53
            DfWritePos Contacts.Company               15 (Font_Bold + iStyle) -1 8.39
            // The content of Contacts.Status can be "A" or "I". We will print
            // "Active" when "A" is the value and "Inactive" when "I" is the value
            If (Contacts.Status = "A") ;
                 DFWriteLnPos "Active"   22 (Font_Bold) -1 1.50
            Else ;
                DFWriteLnPos "Inactive" 22 (Font_Bold + rgb_dRed) -1 1.50
        
            DfWriteLn
            DfWritePos "Title"                  2 (iStyle + Font_Italic) -1 4.26
            DfWriteLnPos Contacts.Title         7 iStyle -1 4.26
            DfWritePos "Address"                2 (iStyle  + Font_Italic) -1 7.46
            DfWriteLnPos Contacts.Address1      7 iStyle -1 7.46
            If (trim(contacts.Address2)<>"") ;
                DFWritelnPos  Contacts.Address2 7  iStyle -1 8
            DFWritelnPos  (Contacts.City - "," * Contacts.ST * Contacts.Postal_Code) 7  iStyle -1 10
            If (trim(contacts.Country)<>"") ;
                DFWritelnPos  Contacts.Country  7  iStyle -1 5
        
            // this will cause nested reports to run
            // In this case, it will print phone numbers and calls for the current key
            Forward Send Body
        
        End_Procedure // Body
        
        // Report_Footer is printed once on the last page before Page_Footer
        Procedure Report_Footer
            // reset the counter so pages will not break before printing the first record
            set piCount to 0
        End_Procedure
        
        Procedure Page_Footer
            String sStart sStop sText sFont sSelection
            Integer iFontSize iStyle
            Handle hoDD
        
            Move "arial" to sFont
            Move 8 to iFontSize
            Move (font_default) to iStyle
        
            Get Server To hoDD
        
            DFFont sFont
            DFFontSize iFontSize
            DFBeginHeader DFPageFooter
                DfHeaderFrame hdr_NoFrame
                DfHeaderPos   hdr_Left
        
                Get psStartLast_Name of hoDD to sStart
                Get psStopLast_Name  of hoDD to sStop
                Get SelectionCriteraDescription "Last Name" sStart sStop to sText
                move sText to sSelection
        
                Get psStartCompany of hoDD to sStart
                Get psStopCompany  of hoDD to sStop
                Get SelectionCriteraDescription "Company" sStart sStop to sText
                If (sSelection <> "" and sText <> "") begin
                    move (sSelection + ", ") to sSelection
                end
                move (sSelection * sText) to sSelection
        
                If (sSelection <> "") begin
                    DfWriteLnPos sSelection 0.1
                end
        
                get SelectedStatus to sText
                DfWritePos sText 0.1
        
                get SortOrder to sText
                DfWriteLnPos sText 5
        
            DFEndHeader
        End_Procedure // Page_Footer
        
        // Page_Bottom is printed last at the bottom margin of each page
        Procedure Page_Bottom
            String sFont
            Integer iFontSize iBorder iStyle
            DateTime dtDT
        
            Move (CurrentDateTime()) to dtDT
        
            Move "arial" to sFont
            Move 10 to iFontSize
            Move (font_default) to iStyle
            Move (rgb_dGrey) to iBorder
        
            DFFont sFont
            DFFontSize iFontSize
            DFBeginHeader DFPageBottom
                DFHeaderFrame Hdr_Margins 0.01 iBorder
                DFHeaderPos Hdr_Left
                DFWriteln ("Report Printed on" * string(dtDT)) (iStyle+Font_Center)
            DFEndHeader
        
        End_Procedure // Page_Bottom
        
        // Returns string with description of selection ranges
        // Pass: Selection title, start value and end value. Return: description
        Function SelectionCriteraDescription string sTitle string sStart string sStop returns String
            String sText
        
            If (sStart=""  AND sStop="")  Function_Return ""
            If (sStart="0" AND sStop="0") Function_Return ""
            If (sStart<>"" AND sStop<>"") Begin
                Move ("between" * sStart * "and" * sStop) to sText
            End
            Else if (sStart > "")Begin
                Move ("starting at" * sStart) to sText
            End
            Else if (sStop > "") Begin
                Move ("up to" * sStop) to sText
            End
            Function_Return (sTitle * sText)
        End_Function
        
        // Returns the selected status
        Function SelectedStatus returns string
            Integer iStatus
            String sStatus
        
            move "Printing " to sStatus
        
            Get Current_Radio of oSelectStatusRG 0 to iStatus
            If (iStatus = 0)      move (sStatus + "all Contacts") to sStatus
            Else If (iStatus = 1) Move (sStatus + "only Active Contacts") to sStatus
            Else                  move (sStatus + "only Inactive Contacts") to sStatus
        
            Function_Return sStatus
        End_Function
        
        // Returns the selected sort order
        Function SortOrder returns string
            Integer iOrder
            String sOrder
        
            Move "Ordered by " to sOrder
        
            Get Current_radio of oOrderRG 0 To iOrder
            If (iOrder = 0)      move (sOrder + "Key") to sOrder
            Else If (iOrder = 1) Move (sOrder + "Last Name") to sOrder
            Else                 move (sOrder + "Company") to sOrder
        
            Function_Return sOrder
        End_Function
        
        //AB-StoreEnd

    End_Object    // oReport

    Procedure StartReport
        Boolean bToPrinter
        Handle hoDD
        String sValue
        String sStart sStop
        Integer iMode
    
        Get Server of oReport To hoDD
    
        // Set all selection critera properties
        Get StartValue of oLast_NameSelection to sValue
        Set psStartLast_Name of hoDD To sValue
    
        Get StopValue of oLast_NameSelection to sValue
        Set psStopLast_Name of hoDD To sValue
    
        Get StartValue of oCompanySelection to sValue
        Set psStartCompany of hoDD To sValue
    
        Get StopValue of oCompanySelection to sValue
        Set psStopCompany of hoDD To sValue
    
    
        // determine if direct print
        Get IsToPrinter of oPrintTo to bToPrinter
        Set OutPut_Device_Mode of oReport to (If(bToPrinter, PRINT_TO_PRINTER, PRINT_TO_WINDOW))
    
        // run the report
        Send Run_Report of oReport
    End_Procedure // StartReport
    
    // Create form object to be used in the Format_String function
    Object oMaskedForm is a Form
        set Form_DataType to Mask_Window
        set Focus_Mode to NonFocusable
    End_Object
    
    // Format string according to the mask passed as parameter
    Function Format_String string sNumber string sMask returns string
        String sMaskedPhone
    
        set Form_Mask of oMaskedForm to sMask
        set Value of oMaskedForm to sNumber
    
        get Masked_Value of oMaskedForm to sMaskedPhone
    
        Function_Return sMaskedPhone
    End_Function
    
End_Object    // oContact_Detail
