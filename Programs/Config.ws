[Workspace]
Home=..\
AppSrcPath=.\AppSrc
AppHTMLPath=.\AppHtml
BitmapPath=.\Bitmaps
IdeSrcPath=.\IdeSrc
DataPath=.\Data
DDSrcPath=.\DdSrc
Description = Contact Management Example
HelpPath=.\Help
ProgramPath=.\Programs
FileList=.\Data\Filelist.cfg
